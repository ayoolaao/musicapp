import React from 'react';
import './app.scss';
import PlayerBar from './components/playerBar/PlayerBar';

import { tracks } from './mockdata/tracks'
import SideNavigation from './components/sideNavigation/SideNavigation';

function App() {
  return (
    <div className="App">
      <section className="nav">
        <SideNavigation />
      </section>
      <main className="main">Main</main>
      <PlayerBar currentlyPlaying={tracks[0]} />
    </div>
  );
}

export default App;
