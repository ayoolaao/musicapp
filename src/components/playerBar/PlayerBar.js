import React from 'react';
import PropTypes from 'prop-types';
import './playerBar.scss';

import { FaStepBackward, FaStepForward, FaPlay, FaVolumeUp } from 'react-icons/fa';

const PlayerBar = ({currentlyPlaying}) => {
  const { name: trackName, artists, album } = currentlyPlaying;
  const artistName = artists[0].name;
  const albumArtUrl = album.images[0].url;

  return (
    <div className="player-bar">
      <div className="player-bar__currently-playing">
        <img className="player-bar__currently-playing__artwork" src={albumArtUrl} alt="album art" />
        <div className="player-bar__currently-playing__info">
          <div className="player-controls__currently-playing__info__title">{trackName}</div>
          <div className="player-controls__currently-playing__info__artist">{artistName}</div>
        </div>
      </div>

      <div className="player-bar__controls">
        <FaStepBackward className="player-bar__controls-button previous" size="1.5em"/>
        <FaPlay className="player-bar__controls-button play-pause" size="1.5em"/>
        <FaStepForward className="player-bar__controls-button next" size="1.5em"/>
      </div>

      <div className="player-bar__seek-bar">
        <div className="player-bar__seek-bar-item">
          <div className="player-bar__seek-bar-item-bg">{}</div>
        </div>
      </div>

      <div className="player-bar__volume">
        <FaVolumeUp className="player-bar__volume-item" size="1.5em"/>
      </div>
    </div>
  );
};

PlayerBar.propTypes = {
  currentlyPlaying: PropTypes.object
};

export default PlayerBar;
