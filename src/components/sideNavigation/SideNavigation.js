import React from 'react';
import './sideNavigation.scss';

import { FaUser, FaRecordVinyl, FaShoppingBag } from 'react-icons/fa';
import { BsMusicNoteList, BsMusicNoteBeamed, BsFillHeartFill, BsFillDisplayFill } from 'react-icons/bs';
import { GoRadioTower } from 'react-icons/go';

const SideNavigation = props => {
  return (
    <div className="side-nav">
      <section className="side-nav__library">
        <div className="side-nav__library-header">Library</div>
        <div className="side-nav__library-items">
          <div className="side-nav__library-items__item playlists">
            <BsMusicNoteList className="side-nav__library-items__item-icon" size="1.5em" />
            <div className="side-nav__library-items__item-label">PlayLists</div>
          </div>
          <div className="side-nav__library-items__item artist active-item">
            <FaUser className="side-nav__library-items__item-icon" size="1.5em" />
            <div className="side-nav__library-items__item-label">Artists</div>
          </div>
          <div className="side-nav__library-items__item albums">
            <FaRecordVinyl className="side-nav__library-items__item-icon" size="1.5em" />
            <div className="side-nav__library-items__item-label">Albums</div>
          </div>
          <div className="side-nav__library-items__item playlists">
            <BsMusicNoteBeamed className="side-nav__library-items__item-icon" size="1.5em" />
            <div className="side-nav__library-items__item-label">Songs</div>
          </div>
        </div>
      </section>

      <section className="side-nav__discover">
        <div className="side-nav__discover-header">Discover</div>
        <div className="side-nav__discover-items">
          <div className="side-nav__discover-items__item store">
            <FaShoppingBag className="side-nav__discover-items__item-icon" size="1.5em" />
            <div className="side-nav__discover-items__item-label">Store</div>
          </div>
          <div className="side-nav__discover-items__item radio">
            <GoRadioTower className="side-nav__discover-items__item-icon" size="1.5em" />
            <div className="side-nav__discover-items__item-label">Radio</div>
          </div>
          <div className="side-nav__discover-items__item for-you">
            <BsFillHeartFill className="side-nav__discover-items__item-icon" size="1.5em" />
            <div className="side-nav__discover-items__item-label">For You</div>
          </div>
          <div className="side-nav__discover-items__item browse">
            <BsFillDisplayFill className="side-nav__discover-items__item-icon" size="1.5em" />
            <div className="side-nav__discover-items__item-label">Browse</div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SideNavigation;
