export const albums = [ {
  "album_type" : "album",
  "artists" : [ {
    "href" : "",
    "id" : "53A0W3U0s8diEn9RhXQhVz",
    "name" : "Keane",
    "type" : "artist",
    "uri" : ""
  } ],
  "copyrights" : [ {
    "text" : "(C) 2013 Universal Island Records, a division of Universal Music Operations Limited",
    "type" : "C"
  }, {
    "text" : "(P) 2013 Universal Island Records, a division of Universal Music Operations Limited",
    "type" : "P"
  } ],
  "external_ids" : {
    "upc" : "00602537518357"
  },
  "genres" : [ ],
  "href" : "",
  "id" : "41MnTivkwTO3UUJ8DrqEJJ",
  "images" : [ {
    "height" : 640,
    "url" : "https://i.scdn.co/image/89b92c6b59131776c0cd8e5df46301ffcf36ed69",
    "width" : 640
  }, {
    "height" : 300,
    "url" : "https://i.scdn.co/image/eb6f0b2594d81f8d9dced193f3e9a3bc4318aedc",
    "width" : 300
  }, {
    "height" : 64,
    "url" : "https://i.scdn.co/image/21e1ebcd7ebd3b679d9d5084bba1e163638b103a",
    "width" : 64
  } ],
  "name" : "The Best Of Keane (Deluxe Edition)",
  "popularity" : 65,
  "release_date" : "2013-11-08",
  "release_date_precision" : "day",
  "tracks" : {
    "href" : "",
    "items" : [ {
      "artists" : [ {
        "href" : "",
        "id" : "53A0W3U0s8diEn9RhXQhVz",
        "name" : "Keane",
        "type" : "artist",
        "uri" : ""
      } ],
      "disc_number" : 1,
      "duration_ms" : 215986,
      "explicit" : false,
      "href" : "",
      "id" : "4r9PmSmbAOOWqaGWLf6M9Q",
      "name" : "Everybody's Changing",
      "track_number" : 1,
      "type" : "track",
      "uri" : "spotify:track:4r9PmSmbAOOWqaGWLf6M9Q"
    }, {
      "artists" : [ {
        "external_urls" : {
          "spotify" : "https://open.spotify.com/artist/53A0W3U0s8diEn9RhXQhVz"
        },
        "href" : "https://api.spotify.com/v1/artists/53A0W3U0s8diEn9RhXQhVz",
        "id" : "53A0W3U0s8diEn9RhXQhVz",
        "name" : "Keane",
        "type" : "artist",
        "uri" : "spotify:artist:53A0W3U0s8diEn9RhXQhVz"
      } ],
      "disc_number" : 1,
      "duration_ms" : 235880,
      "explicit" : false,
      "href" : "",
      "id" : "0HJQD8uqX2Bq5HVdLnd3ep",
      "name" : "Somewhere Only We Know",
      "track_number" : 2,
      "type" : "track",
      "uri" : "spotify:track:0HJQD8uqX2Bq5HVdLnd3ep"
    }],
    "limit" : 50,
    "next" : null,
    "offset" : 0,
    "previous" : null,
    "total" : 9
  },
  "type" : "album",
  "uri" : ""
} ];
