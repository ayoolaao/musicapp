export const tracks = [
  {
    "album": {
      "album_type": "single",
      "artists": [
        {
          "id": "6sFIWsNpZYqfjUpaCgueju",
          "name": "Carly Rae Jepsen",
          "type": "artist",
          "uri": ""
        }
      ],
      "href": "",
      "id": "0tGPJ0bkWOUmH7MEOR77qc",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/966ade7a8c43b72faa53822b74a899c675aaafee",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/107819f5dc557d5d0a4b216781c6ec1b2f3c5ab2",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/5a73a056d0af707b4119a883d87285feda543fbb",
          "width": 64
        }
      ],
      "name": "Cut To The Feeling",
      "release_date": "2017-05-26",
      "release_date_precision": "day",
      "type": "album"
    },
    "artists": [
      {
        "id": "6sFIWsNpZYqfjUpaCgueju",
        "name": "Carly Rae Jepsen",
        "type": "artist"
      }
    ],
    "disc_number": 1,
    "duration_ms": 207959,
    "explicit": false,
    "external_ids": {
      "isrc": "USUM71703861"
    },
    "href": "",
    "id": "11dFghVXANMlKmJXsNCbNl",
    "name": "Cut To The Feeling",
    "popularity": 63,
    "track_number": 1,
    "type": "track",
    "uri": ""
  },
  {
    "album": {
      "album_type": "album",
      "artists": [
        {
          "href": "",
          "id": "6sFIWsNpZYqfjUpaCgueju",
          "name": "Carly Rae Jepsen",
          "type": "artist",
          "uri": ""
        }
      ],
      "href": "",
      "id": "6SSSF9Y6MiPdQoxqBptrR2",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/2fb20bf4c1fb29b503bfc21516ff4b1a334b6372",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/a7b076ed5aa0746a21bc71ab7d2b6ed80dd3ebfe",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/b1d4c7643cf17c06b967b50623d7d93725b31de5",
          "width": 64
        }
      ],
      "name": "Kiss",
      "release_date": "2012-01-01",
      "release_date_precision": "day",
      "type": "album",
      "uri": ""
    },
    "artists": [
      {
        "href": "",
        "id": "6sFIWsNpZYqfjUpaCgueju",
        "name": "Carly Rae Jepsen",
        "type": "artist",
        "uri": ""
      }
    ],
    "disc_number": 1,
    "duration_ms": 193400,
    "explicit": false,
    "external_ids": {
      "isrc": "CAB391100615"
    },
    "href": "",
    "id": "20I6sIOMTCkB6w7ryavxtO",
    "name": "Call Me Maybe",
    "popularity": 74,
    "track_number": 3,
    "type": "track",
    "uri": ""
  },
  {
    "album": {
      "album_type": "album",
      "artists": [
        {
          "href": "",
          "id": "6sFIWsNpZYqfjUpaCgueju",
          "name": "Carly Rae Jepsen",
          "type": "artist",
          "uri": ""
        }
      ],
      "href": "",
      "id": "1DFixLWuPkv3KT3TnV35m3",
      "images": [
        {
          "height": 640,
          "url": "https://i.scdn.co/image/3f65c5400c7f24541bfd48e60f646e6af4d6c666",
          "width": 640
        },
        {
          "height": 300,
          "url": "https://i.scdn.co/image/ff347680d9e62ccc144926377d4769b02a1024dc",
          "width": 300
        },
        {
          "height": 64,
          "url": "https://i.scdn.co/image/c836e14a8ceca89e18012cab295f58ceeba72594",
          "width": 64
        }
      ],
      "name": "Emotion (Deluxe)",
      "release_date": "2015-09-18",
      "release_date_precision": "day",
      "type": "album",
      "uri": ""
    },
    "artists": [
      {
        "href": "",
        "id": "6sFIWsNpZYqfjUpaCgueju",
        "name": "Carly Rae Jepsen",
        "type": "artist",
        "uri": ""
      }
    ],
    "disc_number": 1,
    "duration_ms": 251319,
    "explicit": false,
    "external_ids": {
      "isrc": "USUM71507009"
    },
    "href": "",
    "id": "7xGfFoTpQ2E7fRF5lN10tr",
    "is_local": false,
    "name": "Run Away With Me",
    "popularity": 50,
    "track_number": 1,
    "type": "track",
    "uri": ""
  }
]
